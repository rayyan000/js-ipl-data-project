function calculateExtrasConcededByEachTeamIn2016(matches,deliveries){
    let MatchID2016 = new Set();
    let TeamVsExtras = {};

    for(let CurrentMatch of matches)    {
        if(CurrentMatch["season"] == 2016)    {
            MatchID2016.add(CurrentMatch["id"]);
        }
    }

    for( CurrentDelivery of deliveries) {
        if(MatchID2016.has(CurrentDelivery["match_id"]))    {

            let CurrentBowlingTeam = CurrentDelivery["bowling_team"], Extras = CurrentDelivery["extra_runs"];
            if(TeamVsExtras[CurrentBowlingTeam])  {
                TeamVsExtras[CurrentBowlingTeam] = TeamVsExtras[CurrentBowlingTeam] + parseInt(Extras);
            }   else    {
                TeamVsExtras[CurrentBowlingTeam] = parseInt(Extras);
            }

        }
    }

    console.log("\nExtra runs conceded by each team over 2008 to 2017 are as follows:",TeamVsExtras);
    return TeamVsExtras;

}
module.exports = calculateExtrasConcededByEachTeamIn2016