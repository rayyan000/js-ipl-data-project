function calculateNumberOfMatchesPlayedPerYear(matches) {
    let AnnualCountOfMatchesArray = {};

    for(let CurrentMatch of matches)    {
        let CurrentSeason = CurrentMatch["season"];
        if(AnnualCountOfMatchesArray[CurrentSeason])    {
            AnnualCountOfMatchesArray[CurrentSeason]++;
        } else  {
            AnnualCountOfMatchesArray[CurrentSeason] = 1;
        }
    }

    console.log("Annual count of matches in IPL Seasons are as following: ", AnnualCountOfMatchesArray );
    return AnnualCountOfMatchesArray;
}
module.exports = calculateNumberOfMatchesPlayedPerYear