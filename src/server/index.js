const ConvertCSVToJSON = require("convert-csv-to-json");
const FileSync = require("fs")

let Matches = [], Deliveries = [];

Matches = ConvertCSVToJSON.fieldDelimiter(',').getJsonFromCsv("./src/data/matches.csv");
Deliveries = ConvertCSVToJSON.fieldDelimiter(',').getJsonFromCsv("./src/data/deliveries.csv");

const functionForQuestion1 = require("./calculateNumberOfMatchesPlayedPerYear");
let Answer1 = functionForQuestion1(Matches);
FileSync.writeFileSync("./src/public/output/NumberOfMatchesPlayedPerYear.json",JSON.stringify(Answer1,null,2));

const functionForQuestion2 = require("./calculateNumberOfMatchesWonPerTeaminIPL");
let Answer2 = functionForQuestion2(Matches, Deliveries);
FileSync.writeFileSync("./src/public/output/NumberOfMatchesWonByEachTeaminIPL.json",JSON.stringify(Answer2,null,2));

const functionForQuestion3 = require("./calculateExtrasConcededByEachTeamIn2016");
let Answer3 = functionForQuestion3(Matches, Deliveries);
FileSync.writeFileSync("./src/public/output/ExtrasConcededPerTeamInSeason2016.json",JSON.stringify(Answer3,null,2));

const functionForQuestion4 = require("./listTopTenEconomicalBowlersOf2015");
let Answer4 = functionForQuestion4(Matches, Deliveries);
FileSync.writeFileSync("./src/public/output/TopTenEconomicalBowlersOf2015.json",JSON.stringify(Answer4,null,2));
