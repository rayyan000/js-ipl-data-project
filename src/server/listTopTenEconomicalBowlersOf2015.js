function listTopTenEconomicalBowlersOf2015(Matches, Deliveries){
    const BowlerVsRunsConceded = new Map();
    const BowlerVsNumberOfBallsBowled = new Map();
    const BowlerEconomy = new Map();
    let FirstMatchID = LastMatchID = 0;
    
    for(CurrentMatch of Matches){
        if(CurrentMatch.season == 2015){
            if(FirstMatchID == 0){
                FirstMatchID = CurrentMatch.id;
            }
            LastMatchID = CurrentMatch.id;
        }
    }

    for(Delivery of Deliveries){
        if(Delivery.match_id >= FirstMatchID && Delivery.match_id <= LastMatchID){
            if(BowlerVsNumberOfBallsBowled.has(Delivery.bowler))    {
                BowlerVsNumberOfBallsBowled.set(Delivery.bowler, BowlerVsNumberOfBallsBowled.get(Delivery.bowler) + 1); 
            } else  {
                BowlerVsNumberOfBallsBowled.set(Delivery.bowler, 1)
            }

            if(BowlerVsRunsConceded.has(Delivery.bowler))   {
                BowlerVsRunsConceded.set(Delivery.bowler, BowlerVsRunsConceded.get(Delivery.bowler)+Delivery.total_runs);
            } else  {
                BowlerVsRunsConceded.set(Delivery.bowler, Delivery.total_runs);
            }
        }
    }
    for(bowler of BowlerVsRunsConceded.keys())  {
        let CurrentBowlerEconomy = (BowlerVsRunsConceded.get(bowler) * 6.0 / BowlerVsNumberOfBallsBowled.get(bowler));
        BowlerEconomy.set(bowler, CurrentBowlerEconomy);    
    }

    const BowlersAccordingToEconomyInAscendingOrderRankings = new Map([...BowlerEconomy].sort((a, b) => (a[1] > b[1] ? 1 : -1)));
    
    let RankedBowlerNamesArray = {};
    for (let i = 0; i < 10; i++)   {
        RankedBowlerNamesArray[i+1]= [...BowlersAccordingToEconomyInAscendingOrderRankings][i][0];
    }

    console.log("\nFrom the 2015 IPL Season, top economical bowlers are listed below according to their ranks:");
    console.log(RankedBowlerNamesArray)
    return RankedBowlerNamesArray;
    
}
module.exports = listTopTenEconomicalBowlersOf2015