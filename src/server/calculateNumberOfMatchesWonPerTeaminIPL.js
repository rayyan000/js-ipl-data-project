function calculateNumberOfMatchesWonPerTeaminIPL(matches)   {
    let TeamWinCountAnnual = {};
    
    for(let CurrentMatch of matches)    {
        let CurrentYear = CurrentMatch["season"], CurrentWinners = CurrentMatch["winner"];

        if(TeamWinCountAnnual[CurrentYear])    {

            let TemporaryDictionary = TeamWinCountAnnual[CurrentYear];

            if(TemporaryDictionary[CurrentWinners])   {

                let CurrentTeamWins = TemporaryDictionary[CurrentWinners];
                TemporaryDictionary[CurrentWinners] = CurrentTeamWins + 1;
                
            }   else    {
                TemporaryDictionary[CurrentWinners] = 1;
            }

        }   else    {

            let TemporaryDictionary = {};
            TemporaryDictionary[CurrentWinners] = 1;
            TeamWinCountAnnual[CurrentYear] = TemporaryDictionary;

        }
    }
    console.log("\nAnnual count of wins official for each IPL performing team are as follows: ");
    console.log(TeamWinCountAnnual);
    return TeamWinCountAnnual;
    

}
module.exports = calculateNumberOfMatchesWonPerTeaminIPL